# script

常用脚本汇总（注意：脚本仅供自用，商用后果自负）

## acme证书一键安装脚本
    bash <(curl -L -s https://gitlab.fanqiemb.top/sd19836311/script/raw/master/acme.sh)

## tuic v5协议一键安装脚本（如果是IPV6机器，请删除客户端配置的IP。通过域名进行解析即可。）
    bash <(curl -L -s https://gitlab.fanqiemb.top/sd19836311/script/raw/master/tuic.sh)

## hysteria协议一键安装脚本（证书需要自行添加路径）
    bash <(curl -L -s https://gitlab.fanqiemb.top/sd19836311/script/raw/master/hysteria.sh)
     
## 脚本参考以下大佬技术， 非常感谢大佬们的技术分享
    MisakaNo の 小破站 https://gitlab.com/Misaka-blog